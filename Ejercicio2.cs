using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
Crear un programa que permita crear, cargar y obtener el menor y mayor valor de un vector. 
La obtención del mayor y menor hacerlo en un único método que retorne dichos dos valores.
*/

namespace ConsoleApp1
{
    class Program
    {
        private int[] valor;
        private int x = 1;
        
        public Program()
        {
            Console.Write("Ingresar el tamaño del vector: ");
            int n = int.Parse(Console.ReadLine());
            valor = new int[n];
        }
        
        public void CargarValor()
        {
            
            for (int i = 0; i < valor.Length; i++)
            {
                Console.Write($"Ingresar valor {x}: ");
                valor[i] = int.Parse(Console.ReadLine());
                x++;
            }
        }
        
        public void MayorMenor(out int mayor,out int menor, out int medio)
        {
            mayor = valor[0];
            menor = valor[0];
            medio = valor[0];
            
            for (int i = 1; i < valor.Length; i++)
            {
                if (valor[i] > mayor)
                {
                    mayor = valor[i];
                }
                else if (valor[i] < menor) 
                {
                    menor = valor[i];
                }
                
                // Quise agregar la condicion del valor intermedio :)
                else 
                {
                    if (valor[i] < mayor && valor[i] > menor)
                    {
                        medio = valor[i];
                    }
                }
            }   
        }
        
        static void Main(string[] args)
        {
            
            Program pr = new Program();
            
            pr.CargarValor();
            int may, men, med;
            pr.MayorMenor(out may,out men,out med);
            Console.WriteLine($"El valor más grande es: {may}");
            Console.WriteLine($"El valor del medio es: {med}");
            Console.WriteLine($"El valor más pequeño es: {men}");
        }
    }
}
