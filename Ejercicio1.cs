using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
Crear un metodo que retorne 5 valores random entre 1 y 30 mediante parametros por referencia.
*/

namespace ConsoleApp1
{
    class Program
    {
        
        static void Main(string[] args)
        {
            int num1 = 1;
            int num2 = 30;
            
            RandomRan(ref num1,ref num2);
        }
        
        static void RandomRan(ref int num1, ref int num2)
        {
            int x = 1;
            
            Random ram = new Random();
            int numero;   
            numero = ram.Next(num1,num2);
            for (int i = 0; i < 5; i++)
            {
                numero = ram.Next(num1,num2);
                Console.WriteLine($"Valor aleatorio {x}: {numero}");
                x++;
            }
        }
        
    }
}


